//
//  CollectionViewController.swift
//  Bearnaise ranking
//
//  Created by Joacim Nidén on 2018-05-27.
//  Copyright © 2018 Joacim Nidén. All rights reserved.
//

import UIKit

class CollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
  
  var sauces: [BearnaiseModel] = []
  var longPressGesture: UILongPressGestureRecognizer!
  private let reuseIdentifier = "cell"
  let padding: CGFloat = 10.0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    createCells()
    
    longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
    collectionView?.addGestureRecognizer(longPressGesture)
    
    navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "Logo"))

  }
  
  func createCells() {
    
    sauces = sauces.getList() ?? [
      BearnaiseModel(imageURL: "https://mittkok.expressen.se/wp-content/uploads/2015/12/skrmavbild-2015-12-17-kl--14-45-01-700x700-1.jpg", name: "Home made Bearnaise"),
      BearnaiseModel(imageURL: "https://www.rydbergs.se/wp-content/uploads/2013/12/bearnaisenydesign.jpg", name: "Normal Bearnaise"),
      BearnaiseModel(imageURL: "https://www.rydbergs.se/wp-content/uploads/2013/12/bearnasiechilinydesign.jpg", name: "Chili Bearnaise"),
      BearnaiseModel(imageURL: "https://www.rydbergs.se/wp-content/uploads/2013/12/bearnasiegrillnydesign.jpg", name: "Grill Bearnaise"),
      BearnaiseModel(imageURL: "https://www.onfos.de/media/catalog/product/cache/2/image/600x600/9df78eab33525d08d6e5fb8d27136e95/7/3/7394439500044.jpg", name: "Garlic Bearnaise"),
      BearnaiseModel(imageURL: "https://static.mathem.se/shared/images/products/large/07322550092001_c1c1.jpg", name: "Powder Bearnaise"),
      BearnaiseModel(imageURL: "https://img.coopathome.ch/produkte/880_880/RGB/3022635_001.jpg", name: "Powder Bearnaise"),
      BearnaiseModel(imageURL: "https://y.cdn-expressen.se/images/91/f9/91f9a011108b411bbe2df292fd55d3d3/240.jpg", name: "Truffel Bearnaise")
    ]
  }

  // MARK: UICollectionViewDataSource
  
  override func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }  
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return sauces.count
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return padding
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return padding / 2
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    let width = (collectionView.frame.width - (padding * 3)) / 2.0
    return CGSize(width: width, height: width)
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
    
    cell.bearnaise = sauces[indexPath.row]
    return cell
  }
  
  // MARK: UICollectionViewDelegate
  
  override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
    
    let movedObject = sauces[sourceIndexPath.row]
    sauces.remove(at: sourceIndexPath.row)
    sauces.insert(movedObject, at: destinationIndexPath.row)
    sauces.saveList()
   
  }
  
  @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
    switch(gesture.state) {
      
    case .began:
      guard let selectedIndexPath = collectionView?.indexPathForItem(at: gesture.location(in: collectionView)) else {
        break
      }
      collectionView?.beginInteractiveMovementForItem(at: selectedIndexPath)
    case .changed:
      collectionView?.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
    case .ended:
      collectionView?.endInteractiveMovement()
    default:
      collectionView?.cancelInteractiveMovement()
    }
  }
  
}
