//
//  CollectionViewCell.swift
//  Bearnaise ranking
//
//  Created by Joacim Nidén on 2018-05-27.
//  Copyright © 2018 Joacim Nidén. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var label: UILabel!
  
  var bearnaise: BearnaiseModel? {
    didSet {
      createCell(bearnaise: bearnaise)
    }
  }
  
  func createCell(bearnaise: BearnaiseModel?) {
    guard let bearnaise = bearnaise else {
      return
    }
    
    layer.cornerRadius = 5.0
    layer.masksToBounds = true
    
    DispatchQueue.main.async {
      self.imageView.imageFromURL(bearnaise.imageURL)
      self.label.text = bearnaise.name
    }
  }
}

extension UIImageView {
  
  func imageFromURL(_ urlString: String) {
    guard let url = URL(string: urlString) else {
      dump("no url")
      return
    }
    
    DispatchQueue.global().async {
      if let data = try? Data(contentsOf: url) {
        DispatchQueue.main.async {
          self.image = UIImage(data: data)
        }
      }
    }
  }
}
