//
//  AppDelegate.swift
//  Bearnaise ranking
//
//  Created by Joacim Nidén on 2018-05-24.
//  Copyright © 2018 Joacim Nidén. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    TestFairy.begin("5c93329af6acb04a514dbad1a44dd238a6b93936")
    return true
  }


}

