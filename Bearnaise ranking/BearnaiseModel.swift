//
//  BearnaiseModel.swift
//  Bearnaise ranking
//
//  Created by Joacim Nidén on 2018-05-24.
//  Copyright © 2018 Joacim Nidén. All rights reserved.
//

import UIKit

struct BearnaiseModel: Codable {
  let imageURL: String
  let name: String
}

extension Array where Iterator.Element == BearnaiseModel {
  
  func saveList() {
    let encoder = JSONEncoder()
    if let encoded = try? encoder.encode(self) {
      UserDefaults.standard.set(encoded, forKey: "bearnaise")
    }
  }
  
  func getList() -> [BearnaiseModel]? {
    let decoder = JSONDecoder()
    if let data = UserDefaults.standard.data(forKey: "bearnaise"),
      let sauces = try? decoder.decode([BearnaiseModel].self, from: data) {
        return sauces
    }
    return nil
  }
}
